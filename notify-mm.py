#!/usr/bin/env python3
"""
notify-mm.py
This is a simple script that sends a mattermost notification.
Its called when an artifact could not be uploaded
"""

import argparse
import os
from mattermostdriver import Driver
from pprint import pprint


def sendDM(email: str, message: str):
    user = mm.users.get_user_by_email(email)
    options = [mm.client.userid, user["id"]]
    channel = mm.channels.create_direct_message_channel(options=options)

    mm.posts.create_post(
        options={
            "channel_id": channel["id"],
            "message": message,
        }
    )


if __name__ == "__main__":
    parser = argparse.ArgumentParser(
        description=__doc__, formatter_class=argparse.ArgumentDefaultsHelpFormatter
    )

    parser.add_argument(
        "--email",
        type=str,
        help="The email address of the mattermost user to notify",
        default=os.environ.get("GITLAB_USER_EMAIL"),
    )

    parser.add_argument(
        "--mattermosttoken",
        type=str,
        help="Mattermost webhook token",
        default=os.environ.get("MATTERMOST_TOKEN"),
    )

    args = parser.parse_args()

    # Connect to Mattermost
    mm = Driver(
        {
            "url": "mattermost.eyeo.com",
            "port": 443,
            "token": args.mattermosttoken,
        }
    )
    mm.login()

    url = os.environ.get("CI_JOB_URL")
    msg = f"An artifact upload has failed for your job! Check me! {url}"
    sendDM(args.email, msg)
