#!/bin/bash
# This simple script ensures dependencies are installed before running
# gitlab-archiver.py to upload the current job artifacts to minio

# Where this script is being run from
DIR=$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )

which pip3 &> /dev/null || (sudo apt-get update -qq; sudo apt-get install -qqy python3-pip > /dev/null)
pip3 install --quiet -r ${DIR}/requirements.txt
python3 ${DIR}/gitlab-archiver.py "$@" || python3 ${DIR}/notify-mm.py
