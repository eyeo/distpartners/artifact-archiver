#!/usr/bin/env python3
"""
gitlab-archiver.py
This script is used to copy files defined as gitlab artifacts
to external storage. It should be run after every job
"""

import argparse
import io
import os
import glob
import re
import requests
import sys
import yaml
from unittest.mock import Mock
from pprint import pprint
from minio import Minio
from copy import copy


def getRetentionConfigForProject(project):
    """This returns a dict containing 3 lists that
    represent the retention config for the given project.
    It returns some sane defaults if the project is unknown
    """
    # This is a list of files to **never** copy to the permenant
    # archive bucket. Each item should be a string containing a
    # regex that matches the file. Only the first match is used
    skipArchiveFiles = []

    # A list of strings to be used as regexes on the current branch
    # name. If it matches then the artifacts are also uploaded to
    # the long term storage
    if project == "adblockbrowserandroid":
        branchesToArchive = ["release-.*"]
    elif project == "abpchromium" or project == "chromium-sdk":
        branchesToArchive = ["chromium-.*-vanilla-automated"]

    # The same as branchesToArchive but for git tags. By default
    # assume tags are important
    tagsToArchive = [".*"]

    # Some overrides for specific projects
    if project == "adblockbrowserandroid":
        skipArchiveFiles = [
            "lib_unstripped.zip",
        ]

    # Build it into a dict tor return
    retentionConfig = {
        "skipArchiveFiles": skipArchiveFiles,
        "branchesToArchive": branchesToArchive,
        "tagsToArchive": tagsToArchive,
    }
    return retentionConfig


def shouldArchive(artifactPath, branchname, tagname, project):
    """This takes a file path, branch and tag then returns True
    if the given file should be archived longer than the
    default (ie copied to the "archive forever" bucket)
    """
    retentionConfig = getRetentionConfigForProject(project=project)
    # If the file should be skipped then return False
    for regexstr in retentionConfig["skipArchiveFiles"]:
        if re.match(regexstr, artifactPath):
            return False
    # if it belongs to a branch we are interested in then True
    for regexstr in retentionConfig["branchesToArchive"]:
        if re.match(regexstr, branchname):
            return True
    # check any (optional) tag too
    if tagname:
        for regexstr in retentionConfig["tagsToArchive"]:
            if re.match(regexstr, tagname):
                return True
    # by default we dont want to archive
    return False


def getArtifactsFromJob(job):
    """This uses the gitlab ci config to determine
    the artifacts declared for a job. This includes
    artifacts which are inherited either from a yaml
    anchor or via the gitlab 'extends' keyword.

    Returns the dict of defined artifacts (gitlab format)
    or None if no artifacts are defined

    """
    if not isinstance(job, dict):
        print(f"Skipping {job} which is a {type(job)}")
        return
    if "artifacts" in job.keys():
        return job["artifacts"]
    elif "extends" in job.keys():
        # print('Found extended job')
        basejob = gitlabconfig[job["extends"]]
        return getArtifactsFromJob(basejob)
    else:
        return


def createFriendlyRedirect(branch: str, jobname: str, minioclient: Minio) -> None:
    """This is used to create a friendly text file which
    the user can read to find out where the latest artifact for a branch/job is

    User goes to:
    minio/bucket/branch/jobname

    and gets a text file with a URL to...
    minio/bucket/path/pipelineid/jobid

    """
    source = f"{branch}/{jobname}"

    destination = f"https://{args.miniohost}/{args.bucket}/{args.prefix}"

    encoded = destination.encode("utf-8")
    stream = io.BytesIO(encoded)
    upload_result = minioclient.put_object(args.bucket, source, stream, len(encoded))
    print(f"Alias updated for '{source}' to '{destination}'")


def uploadArtifact(artifactPath, minioclient):
    """This function takes a given artifact (which should be present
    on the local filesystem) and uploads it to minio.

    artifactPath can include wildcards, environment variables and
    will recurse into directories declared as artifacts
    """
    if "$" in artifactPath:
        artifactPath = os.path.expandvars(artifactPath)

    filelist = glob.glob(artifactPath, recursive=True)

    # If the artifact is a folder then include everything inside it
    for dir in copy(filelist):  # Use a copy so we can update the original
        if os.path.isdir(dir):
            filelist.remove(dir)
            for root, _, files in os.walk(dir):
                for file in files:
                    newfile = os.path.join(root, file)
                    filelist.append(newfile)

    if not filelist:
        print(f"No matching files for {artifactPath}")
        return
    else:
        print(f"Uploading artifacts matching path {artifactPath} to minio")

    for filename in filelist:
        # remove leading ./ from filename so not to confuse minio
        if filename.startswith("./"):
            filename = filename[2:]

        if not os.path.isfile(filename):
            print(f"File not found: {filename}")
            continue

        # Upload the file
        upload_result = minioclient.fput_object(
            args.bucket,
            f"{args.prefix}/{filename}",
            filename,
        )

        # Upload to the archive bucket
        if shouldArchive(filename, args.branchname, args.tagname, args.projectname):
            print(
                f"Adding file to permenant archive bucket {args.archive_bucket} {args.branchname}/{filename}"
            )
            archive_upload_result = minioclient.fput_object(
                args.archive_bucket,
                f"{args.branchname}/{filename}",
                filename,
            )

        # Delete artifacts after they have been successfully uploaded
        if args.delete:
            if not args.dryrun:
                print(f"Deleting archived file: {filename}")
                os.remove(filename)
            else:
                print(f"Would delete {filename} in non-dryrun mode")


def main(args):

    if args.dryrun:
        minioclient = Mock(Minio)
        print("Dryrun mode enabled. Nothing will be uploaded to minio")
    else:
        try:
            access_key = os.environ["MINIO_ACCESS_KEY"]
            secret_key = os.environ["MINIO_SECRET_KEY"]
        except KeyError:
            print("MINIO_ACCESS_KEY and MINIO_SECRET_KEY env vars need to be set")
            sys.exit(1)
        minioclient = Minio(args.miniohost, access_key, secret_key)

    global gitlabconfig
    if os.environ.get("GIT_STRATEGY") == "none":
        # Try to download the CI config from gitlab directly
        projectUrl = os.environ.get("CI_REPOSITORY_URL")[:-4]  # remove .git
        ciconfigurl = f"{projectUrl}/-/raw/{args.branchname}/.gitlab-ci.yml"
        print("Downloading CI config from repo directly (no local checkout found)")
        remoteciconfig = requests.get(ciconfigurl)
        gitlabconfig = yaml.load(remoteciconfig.text, Loader=yaml.BaseLoader)
    else:
        with open(args.ciconfig) as gitlabfile:
            gitlabconfig = yaml.load(gitlabfile, Loader=yaml.BaseLoader)

    job = gitlabconfig[args.jobname]
    artifactdict = getArtifactsFromJob(job)

    if artifactdict:
        if "paths" in artifactdict.keys():
            for artifactpath in artifactdict["paths"]:
                uploadArtifact(artifactpath, minioclient)
        else:
            print(f"No artifacts defined for job {args.jobname}")

    print("*" * 80)
    print("    Artifacts uploaded to minio:")
    print(f"https://{args.miniohost}/minio/{args.bucket}/{args.prefix}/")
    print("*" * 80)

    # Add an alias for the branch
    if args.prefix.startswith("eyeo/"):
        createFriendlyRedirect(
            branch=args.branchname, jobname=args.jobname, minioclient=minioclient
        )


if __name__ == "__main__":
    parser = argparse.ArgumentParser(
        description=__doc__, formatter_class=argparse.ArgumentDefaultsHelpFormatter
    )

    parser.add_argument(
        "--miniohost",
        type=str,
        help="The hostname of your minio server",
        default="distpartners-minio-1.uplink.eyeo.it",
    )

    parser.add_argument(
        "--bucket",
        type=str,
        help="Bucket to put objects in.",
        default=os.environ.get("CI_PROJECT_NAME", "artifacts"),
    )

    parser.add_argument(
        "--archive_bucket",
        type=str,
        help="Bucket to copy artifacts for long-term storage",
        default=os.environ.get("CI_PROJECT_NAME", "artifacts") + "-archive",
    )

    parser.add_argument(
        "--prefix",
        type=str,
        help="The path prefix where to store files",
        default=f"{os.environ.get('CI_PROJECT_PATH')}/"
        + f"{os.environ.get('CI_PIPELINE_ID')}/"
        + f"{os.environ.get('CI_JOB_ID')}",
    )

    parser.add_argument(
        "--ciconfig",
        type=str,
        help="ci config file",
        default=os.environ.get("CI_CONFIG_PATH", ".gitlab-ci.yml"),
    )

    parser.add_argument(
        "--jobname",
        type=str,
        help="The job which defines the artifact",
        default=os.environ.get("CI_JOB_NAME"),
    )

    parser.add_argument(
        "--projectname",
        type=str,
        help="The name of the current project.",
        default=os.environ.get("CI_PROJECT_NAME"),
    )

    parser.add_argument(
        "--branchname",
        type=str,
        help="The current git branch name",
        default=os.environ.get(
            "CI_COMMIT_BRANCH", os.environ.get("CI_COMMIT_REF_NAME")
        ),
    )

    parser.add_argument(
        "--tagname",
        type=str,
        help="The current git tag name",
        default=os.environ.get("CI_COMMIT_TAG"),
    )

    parser.add_argument(
        "--dryrun",
        help="Dry run mode - skip the actual upload",
        action="store_true",
    )

    parser.add_argument(
        "--delete",
        help="Delete artifacts after upload. This will stop archived artifacts from being uploaded to gitlab",
        action="store_true",
    )

    args = parser.parse_args()
    main(args)
