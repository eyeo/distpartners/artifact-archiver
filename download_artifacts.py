#!/usr/bin/env python3
"""
This script provides an easy way to download artifacts from
a given gitlab job/pipeline/branch. It supports downloading
from gitlab directly (using the API or job tokens) and from
Minio in cases where artifacts have been archived. A direct
gitlab download URL can also be given and authentication
will be added to retrieve the artifact.

In addition to the command line parameters (see --help)
it uses the following environment variables:

- MINIO_ACCESS_KEY/MINIO_SECRET_KEY are optional but are used
  to connect to minio when set
  
It requires the python packages below and the
unzip command should be present in the PATH.

python-gitlab==2.7.1
minio==7.0.2
requests

"""

import argparse
import os
import gitlab
import subprocess
import re
import requests
from minio import Minio
from urllib.parse import urlparse


def get_pipeline_job(project, pipeline, job_name: str):
    """This function looks through all jobs in the given pipeline and returns
    the one with the given job_name (if its completed)
    """
    for pipeline_job in pipeline.jobs.list():
        # Look for the job and make sure its finished (manual jobs may not have started)
        if pipeline_job.name == job_name and pipeline_job.finished_at:
            return project.jobs.get(pipeline_job.id)
    return None


def get_gitlab_job(
    project, job_id: int, pipeline_id: int, branch_name: str, job_name: str
):
    """This takes (in order of precedence) a gitlab job id, pipeline id or
    branch/tag name and returns the corresponding gitlab job object
    """
    if job_id:
        return project.jobs.get(job_id)
    elif pipeline_id:
        pipeline = project.pipelines.get(pipeline_id)
        return get_pipeline_job(project, pipeline, job_name)
    elif branch_name:
        # Look for the first successful pipeline that contains our job
        for pipeline in project.pipelines.list(ref=branch_name, status="success"):
            job = get_pipeline_job(project, pipeline, job_name)
            if job:
                return job
    return None


def download_artifacts_from_gitlab(job, download_dir=None):
    """Downloads artifacts from the provided gitlab job into the download_dir.
    Uses the gitlab API
    """
    if not download_dir:
        download_dir = args.download_dir
    if not os.path.isdir(download_dir):
        os.makedirs(download_dir, exist_ok=False)

    zip_filename = "___artifacts.zip"
    print(f"Downloading artifacts from gitlab job: {job.web_url}")
    with open(zip_filename, "wb") as f:
        job.artifacts(streamed=True, action=f.write)

    subprocess.run(["unzip", "-bd", download_dir, zip_filename])
    os.unlink(zip_filename)


def download_artifacts_from_minio(project, job, download_dir=None, pattern=".*\.apk"):
    """Downloads artifacts from a minio instance, using the optional `pattern`
    argument to filter out artifact filenames (`pattern` is a regex).
    It derives the path of the object based on the project/pipeline/job ids
    and assumes they were uploaded in accordance with the artifact-archiver project.
    """
    if not download_dir:
        download_dir = args.download_dir
    if not os.path.isdir(download_dir):
        os.makedirs(download_dir, exist_ok=False)

    # Optional access/secret key, None will work on some buckets
    access_key = os.environ.get("MINIO_ACCESS_KEY")
    secret_key = os.environ.get("MINIO_SECRET_KEY")
    minioclient = Minio(args.miniohost, access_key, secret_key)

    def download(bucketname, prefix):
        downloaded = False
        for minio_object in minioclient.list_objects(
            bucket_name=bucketname, prefix=prefix, recursive=True
        ):
            if re.match(pattern, minio_object.object_name):
                print(
                    f"Downloading {minio_object.object_name} from minio bucket {bucketname}"
                )
                outputfile = f"{download_dir}/{minio_object.object_name}"
                if os.path.exists(outputfile):
                    raise FileExistsError(f'{outputfile} already exists!')

                minioclient.fget_object(
                    bucket_name=bucketname,
                    object_name=minio_object.object_name,
                    file_path=outputfile,
                )
                downloaded = True
        return downloaded

    prefix = f"/{project.path_with_namespace}/{job.pipeline['id']}/{job.id}"
    downloaded = download(project.name, prefix)
    if not downloaded and args.branch_name:
        prefix = (
            f"/{args.branch_name}"  # the path to objects is different in the archive
        )
        print(f"Unable to find artifact in normal bucket ({project.name}).")
        print(f"Looking for objects matching '{prefix}' in {project.name}-archive")
        downloaded = download(f"{project.name}-archive", prefix)
    return downloaded


def get_file_from_url(url: str, gitlabhost: str, download_dir=None):
    """This can download things from arbitrary URLs. It automatically
    adds authentication to any gitlab API requests and can derive
    job/pipeline ids from user-facing (non-API) gitlab URLs. This
    means you can give it a url of a pipeline from your browser
    and it will figure out what you want.
    """
    if not download_dir:
        download_dir = args.download_dir

    if not os.path.isdir(download_dir):
        os.makedirs(download_dir, exist_ok=False)

    headers = {}
    if url.startswith(f"{gitlabhost}/api/v4/"):
        # Its an API URL, append the token and we're good
        # Prefer the gitlab_private_token as it has more access
        if not args.gitlab_private_token:
            headers["JOB-TOKEN"] = args.gitlab_job_token
        else:
            headers["PRIVATE-TOKEN"] = args.gitlab_private_token
    elif url.startswith(gitlabhost):
        # Its a random gitlab url, either a job or pipeline
        # Determine the id from the url and re-run main with the args
        regex = re.compile(
            ".*-/(pipelines/(?P<pipeline_id>[0-9]*)|jobs/(?P<job_id>[0-9]*))$"
        )
        matches = regex.match(url)
        if matches:
            matches = matches.groupdict()
            args.download_url = None
            args.job_id = matches["job_id"]
            args.pipeline_id = matches["pipeline_id"]
            return main(args)

    parsedUrl = urlparse(url)
    filename = os.path.basename(parsedUrl.path)
    outputfile = download_dir + "/" + filename
    if os.path.exists(outputfile):
        raise FileExistsError(f'{outputfile} already exists!')

    print(f"Downloading artifact from URL: {url}")
    with open(outputfile, "wb") as fd:
        r = requests.get(url, headers=headers, stream=True)
        r.raise_for_status()
        for chunk in r.iter_content(chunk_size=128):
            fd.write(chunk)
    return None


def get_artifact_with_job_token(args):
    """If the user only has a job token (CI_JOB_TOKEN) then
    we have *really* limited API access..
    https://docs.gitlab.com/ee/api/README.html#gitlab-cicd-job-token
    If we know both the project and job ids then we should be able
    to get the artifacts by hitting a specific API endpoint directly.
    It's not as nice as properly using the API but saves the user having
    to generate a personal or project access token
    """
    print("WARNING: job tokens have *very* limited API access")
    if not args.job_id:
        print("You need to provide a private token or specify a job id")
        exit(1)
    url = f"{args.gitlabhost}/api/v4/projects/{args.project_id}/jobs/{args.job_id}/artifacts"
    get_file_from_url(url, args.gitlabhost, args.download_dir)
    subprocess.run(
        ["unzip", "-bd", args.download_dir, f"{args.download_dir}/artifacts"]
    )
    os.unlink(f"{args.download_dir}/artifacts")


def main(args):
    if args.branches_or_tags_file:
        with open(args.branches_or_tags_file, "r") as urlfile:
            for index, url in enumerate(urlfile.readlines()):
                url = url.strip()
                get_file_from_url(
                    url, args.gitlabhost, download_dir=f"{args.download_dir}/{index}"
                )
        exit()

    if args.download_url:
        get_file_from_url(args.download_url, args.gitlabhost)
        exit()

    # we have to go via gitlab
    if not args.gitlab_private_token:
        get_artifact_with_job_token(args)
        exit()

    # We have a proper API token. yay.
    gl = gitlab.Gitlab(
        args.gitlabhost,
        private_token=args.gitlab_private_token,
    )
    project = gl.projects.get(args.project_id)

    if args.use_default_branch:
        args.branch_name = project.default_branch
        print(f"Using default branch {args.branch_name}")

    job = get_gitlab_job(
        project, args.job_id, args.pipeline_id, args.branch_name, args.job_name
    )

    if not job:
        print(f"Could not find a matching job for {args.job_name}")
        exit(1)

    if hasattr(job, "artifacts_file"):
        # available on gitlab
        download_artifacts_from_gitlab(job)
    else:
        # artifacts have expired
        download_artifacts_from_minio(project, job)


if __name__ == "__main__":
    parser = argparse.ArgumentParser(
        description=__doc__, formatter_class=argparse.RawDescriptionHelpFormatter
    )

    parser.add_argument(
        "--project-id",
        type=int,
        help="(int) The project ID to download from",
        default=os.environ.get("CI_PROJECT_ID"),
    )

    group = parser.add_mutually_exclusive_group(required=True)

    group.add_argument(
        "--pipeline-id",
        type=int,
        help="(int) The pipeline ID to download artifacts from",
    )

    group.add_argument(
        "--branch-name",
        type=str,
        help="(str) The branch name to download artifacts from",
    )

    group.add_argument(
        "--use-default-branch",
        action="store_true",
        help="Download artifacts from the project's default branch",
    )

    group.add_argument(
        "--job-id", type=int, help="(int) The job ID to download artifacts from"
    )

    group.add_argument(
        "--download-url",
        type=str,
        help="A URL to download the file. Authentication will automatically be added",
    )

    group.add_argument(
        "--branches-or-tags-file",
        type=str,
        help="Read a list of URLs from a given file",
    )
    parser.add_argument(
        "--download-dir",
        type=str,
        help="The directory to save the downloaded artifacts",
        default="artifacts",
    )

    parser.add_argument(
        "--miniohost",
        type=str,
        help="The hostname of your minio server",
        default="distpartners-minio-1.uplink.eyeo.it",
    )

    parser.add_argument(
        "--gitlabhost",
        type=str,
        help="The hostname of your gitlab server, including https://",
        default=os.environ.get("CI_SERVER_URL"),
    )

    parser.add_argument(
        "--gitlab-job-token",
        type=str,
        default=os.environ.get("CI_JOB_TOKEN"),
        help="A job token for authentication. Note that these are extremely limited.",
    )

    parser.add_argument(
        "--gitlab-private-token",
        type=str,
        help="Private token for authentication",
    )

    parser.add_argument(
        "--job-name",
        type=str,
        help="The name of the job that produces your artifact",
        default="build_arm_release",
    )
    args = parser.parse_args()
    main(args)
