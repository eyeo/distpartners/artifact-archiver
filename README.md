# artifact-archiver

This repo contains scripts required to archive gitlab artifacts outside of gitlab, using
S3-compatible object storage (currently [Minio](https://min.io/)).

It aims to solve two problems:
 - The expense of saving large artifacts for a long time on gitlab.com vs archiving on
storage you already have
 - Some of the inflexibility introduced by the gitlab yaml, specifically only being able to
have a single retention policy (`expire_in`) for all artifacts of a job.

The basic behavior of the script is to upload any artifacts for the current job (as defined in the
standard `.gitlab-ci.yaml` file) to a bucket in a minio instance. In addition, if the current
branch/tag/file is declared as being important (by the `getRetentionConfigForProject` function)
then it is also copied to an "archive" bucket. It is expected that the standard bucket has
a [bucket lifecycle](https://docs.min.io/docs/minio-bucket-lifecycle-guide.html) defined to
automatically delete objects after a set period of time and the "archive" bucket keeps objects
for longer (maybe indefinitely). These are **not** managed by this script.

## Usage

The quickest and easiest way to enable this functionality for a project is to add a one-liner
to checkout and run the `bootstrap.sh` script at the end of any job that generates artifacts:

```
git clone https://gitlab.com/eyeo/distpartners/artifact-archiver.git && artifact-archiver/bootstrap.sh
```

The `bootstrap.sh` script installs any python dependencies and calls the main `gitlab-archiver.py` script.
Note that any arguments passed to `bootstrap.sh` are automatically passed over to `gitlab-archiver.py`.

This clone/bootstrap line should be added to your project's `.gitlab-ci.yaml` either at the very end of the
`script` section or as part of the `after_script` (this is useful when templating). Note that there are
a few [caveats](https://docs.gitlab.com/ee/ci/yaml/#after_script) when running as part of the `after_script`,
specifically any script failures in the `after_script` will **not** fail the corresponding job.


### Global Configuration

Since this project is tightly coupled to running in a very specific environment (Gitlab CI) it
relies heavily on the [predefined variables](https://docs.gitlab.com/ee/ci/variables/predefined_variables.html)
setup automatically during CI runs to derive its configuration. These can be overridden by setting
their equivalent command line options (see `./gitlab-archiver.py --help`).

In addition to the default gitlab environment variables, both `MINIO_ACCESS_KEY` and `MINIO_SECRET_KEY` must be
set. For security purposes, they should be declared at either the gitlab project or group level and should
be [masked](https://docs.gitlab.com/ee/ci/variables/#mask-a-cicd-variable)/[protected](https://docs.gitlab.com/ee/ci/variables/#protect-a-cicd-variable) accordingly.


### Artifact Definition

The script uses the standard [`artifacts:paths`](https://docs.gitlab.com/ee/ci/yaml/README.html#artifactspaths)
section from `.gitlab-ci.yaml` to define artifact files.
It maintains support for things like variables, wildcards and yaml anchors to determine an artifact filename.

It does **not** use the `expire_in` keyword for anything and if you intend to reduce storage usage on
gitlab then this should be set to a low value. For maximum storage efficiency run with the `--delete` flag
which automatically deletes artifacts from the filesystem after successful archiving to minio, this should
prevent gitlab from archiving anything besides failed uploads.

## Downloading Artifacts

Also in this repo is [download_artifacts.py](download_artifacts.py) which provides an easy way to download artifacts
from a given gitlab job/pipeline/branch. It supports downloading from gitlab directly (using the API or job tokens)
and from Minio in cases where artifacts have been archived. A direct gitlab download URL can also be given and 
authentication will be added to retrieve the artifact.

See `./download_artifacts.py --help` for full usage.
